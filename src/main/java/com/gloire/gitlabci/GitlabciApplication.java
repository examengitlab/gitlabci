package com.gloire.gitlabci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabciApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabciApplication.class, args);
	}

}
