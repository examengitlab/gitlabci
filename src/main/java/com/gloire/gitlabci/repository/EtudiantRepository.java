package com.gloire.gitlabci.repository;

import com.gloire.gitlabci.entity.Etudiant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtudiantRepository extends JpaRepository<Etudiant, Integer> {
}

