FROM openjdk:17-jdk-slim

ADD target/gitlabci.jar gitlabci.jar

ENTRYPOINT ["java", "-jar","gitlabci.jar"]